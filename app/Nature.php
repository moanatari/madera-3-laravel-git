<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nature extends Model {

	protected $table = 'natures';
	public $timestamps = true;
	protected $fillable = array('name', 'characteristic', 'unit');

	public function finishes()
	{
		return $this->hasMany('App\Finish');
	}

}