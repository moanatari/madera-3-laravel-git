<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleExample extends Model {

	protected $table = 'module_example';
	public $timestamps = true;
	protected $fillable = array('name', 'length', 'module_id', 'example_id', 'parent_id');

}