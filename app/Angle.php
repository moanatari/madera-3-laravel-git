<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Angle extends Model {

	protected $table = 'angles';
	public $timestamps = true;
	protected $fillable = array('name', 'price');

}