<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insulation extends Model {

	protected $table = 'insulations';
	public $timestamps = true;
	protected $fillable = array('name', 'price');

}