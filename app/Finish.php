<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finish extends Model {

	protected $table = 'finishes';
	public $timestamps = true;
	protected $fillable = array('name', 'price', 'nature_id');

}