<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

	protected $table = 'orders';
	public $timestamps = true;
	protected $fillable = array('state', 'advancement');

	public function project()
	{
		return $this->belongsTo('App\Project');
	}

	public function rows()
	{
		return $this->hasMany('App\Row');
	}

	public function modules()
	{
		return $this->belongsToMany('App\Module')->orderBy('stage')->withPivot('id','name','length','stage','parent_id','discount');
	}

    public function getTotalPrice()
    {
        $modules = $this->modules;
        $totalPrice = 0;
        //dd($modules);
        foreach ($modules as $module)
        {
            $currentPrice = $module->getModulePrice();
            $totalPrice = $totalPrice + $currentPrice;
        }


        return $totalPrice;
    }

//    public function getTotalPrice()
//    {
//        $modules = $this->modules;
//        $totalPrice = 0;
//        //dd($modules);
//        foreach ($modules as $module)
//        {
//            $currentPrice = $module->price;
//
//            if ($module->angle instanceof Angle)
//            {
//                $currentPrice = $currentPrice + $module->angle->price;
//            }
//
//            if ($module->insulation instanceof Insulation)
//            {
//                $currentPrice = $currentPrice + $module->insulation->price;
//            }
//
//            if ($module->finish instanceof Finish)
//            {
//                $currentPrice = $currentPrice + $module->finish->price;
//            }
//
//            if ($module->cover instanceof Cover)
//            {
//                $currentPrice = $currentPrice + $module->cover->price;
//            }
//
//            if ($module->frame instanceof Frame)
//            {
//                $currentPrice = $currentPrice + $module->frame->price;
//            }
//
//
//
//            $currentPrice = $currentPrice * $module->pivot->length;
//            $totalPrice = $totalPrice + $currentPrice;
//        }
//
//
//        return $totalPrice;
//    }

}