<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Range extends Model {

	protected $table = 'ranges';
	public $timestamps = true;
	protected $fillable = array('name');

	public function examples()
	{
		return $this->hasMany('App\Example');
	}

}