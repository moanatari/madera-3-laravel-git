<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frame extends Model {

	protected $table = 'frames';
	public $timestamps = true;
	protected $fillable = array('name', 'price');

}