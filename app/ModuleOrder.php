<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleOrder extends Model {

	protected $table = 'module_order';
	public $timestamps = true;
	protected $fillable = array('name', 'length', 'module_id', 'order_id', 'parent_id');

//	public function childs()
//    {
//        return $this->hasMany('App\ModuleOrder', 'parent_id');
//    }

}