<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model {

	protected $table = 'modules';
	public $timestamps = true;
	protected $fillable = array('name', 'price', 'coupePrincipe', 'cctp', 'angle_id', 'insulation_id', 'finish_id', 'frame_id', 'nature_id');

	public function getModulePrice()
    {
        $currentPrice = $this->getUnitPrice();


        $currentPrice = $currentPrice * $this->pivot->length;

        if ($this->pivot->discount != 0)
        {
            $discount = $this->pivot->discount *0.01;
            $discount = $currentPrice * $discount;
            $currentPrice = $currentPrice - $discount;
        }

        return $currentPrice;
    }
//	public function getModulePrice()
//    {
//        $currentPrice = $this->price;
//
//        if ($this->angle instanceof Angle)
//        {
//            $currentPrice = $currentPrice + $this->angle->price;
//        }
//
//        if ($this->insulation instanceof Insulation)
//        {
//            $currentPrice = $currentPrice + $this->insulation->price;
//        }
//
//        if ($this->finish instanceof Finish)
//        {
//            $currentPrice = $currentPrice + $this->finish->price;
//        }
//
//        if ($this->cover instanceof Cover)
//        {
//            $currentPrice = $currentPrice + $this->cover->price;
//        }
//
//        if ($this->frame instanceof Frame)
//        {
//            $currentPrice = $currentPrice + $this->frame->price;
//        }
//
//        $currentPrice = $currentPrice * $this->pivot->length;
//
//        return $currentPrice;
//    }

    public function getUnitPrice()
    {
        $currentPrice = $this->price;

        if ($this->angle instanceof Angle)
        {
            $currentPrice = $currentPrice + $this->angle->price;
        }

        if ($this->insulation instanceof Insulation)
        {
            $currentPrice = $currentPrice + $this->insulation->price;
        }

        if ($this->finish instanceof Finish)
        {
            $currentPrice = $currentPrice + $this->finish->price;
        }

        if ($this->cover instanceof Cover)
        {
            $currentPrice = $currentPrice + $this->cover->price;
        }

        if ($this->frame instanceof Frame)
        {
            $currentPrice = $currentPrice + $this->frame->price;
        }

        return $currentPrice;
    }

	public function examples()
	{
		return $this->belongsToMany('App\Example');
	}

	public function angle()
	{
		return $this->belongsTo('App\Angle');
	}

	public function insulation()
	{
		return $this->belongsTo('App\Insulation');
	}

	public function finish()
	{
		return $this->belongsTo('App\Finish');
	}

	public function cover()
	{
		return $this->belongsTo('App\Cover');
	}

	public function frame()
	{
		return $this->belongsTo('App\Frame');
	}

	public function nature()
	{
		return $this->belongsTo('App\Nature');
	}

	public function orders()
	{
		return $this->belongsToMany('App\Order');
	}

}