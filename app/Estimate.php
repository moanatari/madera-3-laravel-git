<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estimate extends Model {

	protected $table = 'estimates';
	public $timestamps = true;
	protected $fillable = array('order_id', 'discount', 'price');
	protected $visible = array('days');

	public function order()
	{
		return $this->belongsTo('App\Order');
	}

	public function rows()
	{
		return $this->hasMany('App\Row');
	}

}