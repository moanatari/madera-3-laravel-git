<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {

	protected $table = 'customers';
	public $timestamps = true;
	protected $fillable = array('name', 'firstname', 'phone', 'mail', 'address', 'postalCode', 'city');

	public function projects()
	{
		return $this->hasMany('App\Project');
	}

}