<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Row extends Model {

	protected $table = 'rows';
	public $timestamps = true;
    protected $fillable = array('description', 'quantity', 'discount', 'price', 'module_id', 'estimate_id', 'stage', 'nature', 'natureUnit', 'unitPrice');

    public function estimate()
    {
        return $this->belongsTo('App\Estimate');
    }

}