<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Example extends Model {

	protected $table = 'examples';
	public $timestamps = true;
	protected $fillable = array('name', 'range_id');

	public function range()
	{
		return $this->belongsTo('App\Range');
	}

	public function modules()
	{
		return $this->belongsToMany('App\Module')->withPivot('name','length','stage','parent_id');
	}

	public function getTotalPrice()
    {
        $modules = $this->modules;
        $totalPrice = 0;
        //dd($modules);
        foreach ($modules as $module)
        {
            $currentPrice = $module->price;

            if ($module->angle instanceof Angle)
            {
                $currentPrice = $currentPrice + $module->angle->price;
            }

            if ($module->insulation instanceof Insulation)
            {
                $currentPrice = $currentPrice + $module->insulation->price;
            }

            if ($module->finish instanceof Finish)
            {
                $currentPrice = $currentPrice + $module->finish->price;
            }

            if ($module->cover instanceof Cover)
            {
                $currentPrice = $currentPrice + $module->cover->price;
            }

            if ($module->frame instanceof Frame)
            {
                $currentPrice = $currentPrice + $module->frame->price;
            }



            $currentPrice = $currentPrice * $module->pivot->length;
            $totalPrice = $totalPrice + $currentPrice;
        }


        return $totalPrice;
    }

}