<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cover extends Model {

	protected $table = 'covers';
	public $timestamps = true;
	protected $fillable = array('name', 'price');

}