<?php 

namespace App\Http\Controllers;

use App\Customer;
use App\Estimate;
use App\Order;
use App\Row;
use Illuminate\Http\Request;

class EstimateController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $estimate = Estimate::findOrFail($id);
    $customer = Customer::findOrFail($estimate->customer_id);
    return view('estimate.show', ['estimate' => $estimate, 'customer' => $customer]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }

    public function validateEstimate($id)
    {
        //Return view for validate devis avec percentage is if il want.

        $order = Order::findOrFail($id);

        return view('estimate.validateEstimate', ['order_id' => $order->id]);
    }

    public function storeEstimate(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required|integer',
            'discount' => 'required|integer|between:0,100',
            'days' => 'required|integer|min:1',
        ]);

        $order = Order::findOrFail($request->order_id);

        $estimate = new Estimate();
        $estimate->order_id = $order->id;
        $estimate->project_id = $order->project_id;
        $estimate->customer_id = $order->project->customer_id;
        $estimate->discount = $request->discount;
        $estimate->days = $request->days;
        if ($request->discount != 0)
        {
            $discount = $request->discount *0.01;
            $totalprice = $order->getTotalPrice();
            $discount =  $totalprice * $discount;
            $estimate->price = $totalprice - $discount;
        }
        else
        {
            $estimate->price = $order->getTotalPrice();
        }

        $estimate->save();

        foreach ($order->modules as $module)
        {
            $row = new Row();
            $row->description = $module->pivot->name;
            $row->quantity = $module->pivot->length;
            $row->discount = $module->pivot->discount;
            $row->price = $module->getModulePrice();
            $row->unitPrice = $module->getUnitPrice();
            $row->stage = $module->pivot->stage;
            $row->nature = $module->nature->name;
            $row->natureUnit = $module->nature->unit;
            $row->module_id = $module->id;
            $row->estimate_id = $estimate->id;
            $row->save();
        }

        return redirect()->route('estimate.show', $estimate->id);
    }
  
}

?>