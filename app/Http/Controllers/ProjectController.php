<?php 

namespace App\Http\Controllers;
use App\Estimate;
use App\Project;
use App\Customer;
use App\Order;
use Illuminate\Http\Request;

class ProjectController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $projects = Project::orderBy('created_at','desc')->get();
    return view('project.index', ['projects' => $projects]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      $projectCount = Project::all()->count();
      $projectCount = $projectCount + 1;
    return view('project.create', ['count' => $projectCount]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $this->validate($request, [
          'name' => 'required|max:255',
          'reference' => 'max:255',
      ]);

      $project = new Project();
      $project->name = $request->name;
      $project->reference = $request->reference;
      $project->save();

      return redirect()->route('project.show', ['id' => $project->id]);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $project = Project::with('customer')->findOrFail($id);
    $orders = Order::where('project_id',$id)->get();
    $estimates = Estimate::where('project_id',$id)->get();
    return view('project.show', ["project" => $project, 'orders' => $orders, 'estimates' => $estimates]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $project = Project::findOrFail($id);
    return view('project.edit', ['project' => $project]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id, Request $request)
  {
      $this->validate($request, [
          'name' => 'required|max:255',
          'reference' => 'max:255',
      ]);

    $project = Project::findOrFail($id);
    $project->name = $request->name;
    $project->reference = $request->reference;
    $project->save();

    return redirect()->route('project.show', $project->id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }

  public function createCustomer($id)
  {
      return view('project.createCustomer', ['project_id' => $id]);
  }

  public function storeCustomer(Request $request)
  {
      $this->validate($request, [
          'name' => 'required|max:255',
          'firstname' => 'required|max:255',
          'phone' => 'required|max:255',
          'mail' => 'required|max:255',
          'address' => 'required|max:255',
          'postalCode' => 'required|max:5',
          'city' => 'required|max:255',
      ]);

      $project = Project::findOrFail($request->project_id);

      $customer = new Customer();
      $customer->name = $request->name;
      $customer->firstname = $request->firstname;
      $customer->phone = $request->phone;
      $customer->mail = $request->mail;
      $customer->address = $request->address;
      $customer->postalCode = $request->postalCode;
      $customer->city = $request->city;

      $customer->save();
      $project->customer_id = $customer->id;
      $project->save();

      return redirect()->route('project.show',$project->id);
  }

  public function selectCustomer($id)
  {
      $customers = Customer::all();
      return view('project.selectCustomer', ['project_id' => $id, 'customers' => $customers]);
  }

  public function storeSelectedCustomer(Request $request)
  {
      $this->validate($request, [
          'customer_id' => 'required|integer',
      ]);

      $project = Project::findOrFail($request->project_id);
      $project->customer_id = $request->customer_id;
      $project->save();

      return redirect()->route('project.show', $project->id);
  }

}

?>