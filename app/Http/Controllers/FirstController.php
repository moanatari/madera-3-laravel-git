<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class FirstController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::orderBy('created_at', 'desc')->get();
        return view('home', ['projects' => $projects]);
    }
}
