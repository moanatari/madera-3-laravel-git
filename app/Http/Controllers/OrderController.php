<?php 

namespace App\Http\Controllers;
use App\Example;
use App\Module;
use App\ModuleOrder;
use App\Nature;
use App\Order;
use App\Project;
use Illuminate\Http\Request;


class OrderController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

  public function show($id)
  {

    $order = Order::with('modules')->findOrFail($id);
    //dump($order->modules[0]->pivot);
    return view('order.show', ['order' => $order]);
  }

  public function selectExample($id)
  {
      $modeles = Example::with('range')->get();
      return view('order.selectExample',['modeles' => $modeles, 'project_id' => $id]);
  }

  public function storeSelectedExample(Request $request)
  {


      $order = new Order();
      $order->project_id = $request->project_id;
      $order->state = "state";
      $order->advancement = "advancement";
      $order->save();
      $example = Example::with('modules')->findOrFail($request->example_id);

      foreach ($example->modules as $module)
      {
          $mOrder = new ModuleOrder();
          $mOrder->name = $module->pivot->name;
          $mOrder->length = $module->pivot->length;
          $mOrder->stage = $module->pivot->stage;
          $mOrder->parent_id = $module->pivot->parent_id;
          $mOrder->module_id = $module->pivot->module_id;
          $mOrder->order_id = $order->id;
          $mOrder->save();

      }

      return redirect()->route('order.show', $order->id);
  }

  public function selectNature($order_id)
  {
      $natures = Nature::all();
    return view('order.selectnature', ['order_id' => $order_id, 'natures' => $natures]);
  }

  public function passCreateModule(Request $request)
  {
      return redirect()->route('order.createModule',['order_id' => $request->order_id, 'nature_id' => $request->nature_id]);
  }

  public function createModule($order_id, $nature_id)
  {

      $modules = Module::where('nature_id', $nature_id)->get();
    return view('order.createModule', ['order_id' => $order_id, 'nature_id' => $nature_id, 'modules' => $modules]);
  }

  public function storeCreatedModule(Request $request)
  {

      $this->validate($request, [
          'name' => 'required|max:255',
          'length' => 'required|numeric|min:1',
          'stage' => 'required|integer',
      ]);

      $moduleOrder = new ModuleOrder();


      $moduleOrder->name = $request->name;
      $moduleOrder->length = $request->length;
      $moduleOrder->module_id = $request->module_id;
      $moduleOrder->order_id = $request->order_id;
      $moduleOrder->stage = $request->stage;
      $moduleOrder->save();

      return redirect()->route('order.show', $request->order_id);
  }

  public function editModule($id)
  {
      $moduleOrder = ModuleOrder::findOrFail($id);
      $natureOfModuleOrder = Module::findOrfail($moduleOrder->module_id)->nature_id;
      $modules = Module::where('nature_id', $natureOfModuleOrder)->get();
      return view('order.editModule', ['nature_id' => $natureOfModuleOrder, 'modules' => $modules, 'moduleOrder' => $moduleOrder]);
  }

  public function updateEditedModule(Request $request)
  {
      $this->validate($request, [
          'name' => 'required|max:255',
          'length' => 'required|integer|min:1',
          'module_id' => 'required|integer',
          'discount' => 'required|integer|between:0,100',
          'stage' => 'required|integer',
      ]);

    $moduleOrder = ModuleOrder::findOrFail($request->moduleOrder_id);

    $moduleOrder->name = $request->name;
    $moduleOrder->length = $request->length;
    $moduleOrder->stage = $request->stage;
    $moduleOrder->module_id = $request->module_id;
    $moduleOrder->discount = $request->discount;
    $moduleOrder->save();

    return redirect()->route('order.show', $moduleOrder->order_id);


  }

  public function confirmDeleteModule($moduleExample_id)
  {
      $module = ModuleOrder::findOrFail($moduleExample_id);

      return view('order.confirmDeleteModule', ['module' => $module]);

  }

  public function deleteModule($moduleExample_id)
  {
    $module = ModuleOrder::findOrFail($moduleExample_id);
    $module->delete();

    return redirect()->route('order.show', $module->order_id);
  }
  
}

?>