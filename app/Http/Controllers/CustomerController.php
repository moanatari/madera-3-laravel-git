<?php 

namespace App\Http\Controllers;

use App\Customer;
use App\Estimate;
use Illuminate\Http\Request;

class CustomerController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $customers = Customer::all();
    return view('customer.index', ['customers' => $customers]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('customer.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $this->validate($request, [
          'name' => 'required|max:255',
          'firstname' => 'required|max:255',
          'phone' => 'required|max:255',
          'mail' => 'required|max:255',
          'address' => 'required|max:255',
          'postalCode' => 'required|max:5',
          'city' => 'required|max:255',
      ]);

      $customer = new Customer();
      $customer->name = $request->name;
      $customer->firstname = $request->firstname;
      $customer->phone = $request->phone;
      $customer->mail = $request->mail;
      $customer->address = $request->address;
      $customer->postalCode = $request->postalCode;
      $customer->city = $request->city;

      $customer->save();

      return redirect()->route('customer.show', $customer->id);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
      $customer = Customer::findOrFail($id);
      $estimates = Estimate::where('customer_id', $id)->get();

      return view('customer.show', ['customer' => $customer, 'estimates' => $estimates]);
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $customer = Customer::findOrFail($id);
    return view('customer.edit', ['customer' => $customer]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
      $this->validate($request, [
          'name' => 'required|max:255',
          'firstname' => 'required|max:255',
          'phone' => 'required|max:255',
          'mail' => 'required|max:255',
          'address' => 'required|max:255',
          'postalCode' => 'required|max:5',
          'city' => 'required|max:255',
      ]);

      $customer = Customer::findOrFail($id);

      $customer->name = $request->name;
      $customer->firstname = $request->firstname;
      $customer->phone = $request->phone;
      $customer->mail = $request->mail;
      $customer->address = $request->address;
      $customer->postalCode = $request->postalCode;
      $customer->city = $request->city;

      $customer->save();

      return redirect()->route('customer.show', $customer->id);

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>