<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	protected $table = 'projects';
	public $timestamps = true;
	protected $fillable = array('name', 'reference', 'customer_id');

	public function orders()
	{
		return $this->hasMany('App\Order');
	}

	public function customer()
	{
		return $this->belongsTo('App\Customer');
	}

}