<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRowsTable extends Migration {

	public function up()
	{
		Schema::create('rows', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('description')->nullable();
			$table->string('nature')->nullable();
			$table->string('natureUnit')->nullable();
			$table->integer('quantity')->nullable();
			$table->integer('stage')->nullable()->default('0');
			$table->float('discount')->nullable();
			$table->float('price')->nullable();
			$table->float('unitPrice')->nullable();
			$table->integer('module_id')->unsigned()->nullable();
			$table->integer('estimate_id')->unsigned()->nullable();
		});
	}

	public function down()
	{
		Schema::drop('rows');
	}
}