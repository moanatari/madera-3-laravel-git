<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('orders', function(Blueprint $table) {
			$table->foreign('project_id')->references('id')->on('projects')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('projects', function(Blueprint $table) {
			$table->foreign('customer_id')->references('id')->on('customers')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('rows', function(Blueprint $table) {
			$table->foreign('estimate_id')->references('id')->on('estimates')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->foreign('angle_id')->references('id')->on('angles')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->foreign('insulation_id')->references('id')->on('insulations')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->foreign('finish_id')->references('id')->on('finishes')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->foreign('frame_id')->references('id')->on('frames')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->foreign('nature_id')->references('id')->on('natures')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('examples', function(Blueprint $table) {
			$table->foreign('range_id')->references('id')->on('ranges')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('example_module', function(Blueprint $table) {
			$table->foreign('module_id')->references('id')->on('modules')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('example_module', function(Blueprint $table) {
			$table->foreign('example_id')->references('id')->on('examples')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('finishes', function(Blueprint $table) {
			$table->foreign('nature_id')->references('id')->on('natures')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('frames', function(Blueprint $table) {
			$table->foreign('nature_id')->references('id')->on('natures')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('module_order', function(Blueprint $table) {
			$table->foreign('module_id')->references('id')->on('modules')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('module_order', function(Blueprint $table) {
			$table->foreign('order_id')->references('id')->on('orders')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('estimates', function(Blueprint $table) {
			$table->foreign('order_id')->references('id')->on('orders')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
        Schema::table('estimates', function(Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::table('estimates', function(Blueprint $table) {
            $table->foreign('customer_id')->references('id')->on('customers')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
	}

	public function down()
	{
		Schema::table('orders', function(Blueprint $table) {
			$table->dropForeign('orders_project_id_foreign');
		});
		Schema::table('projects', function(Blueprint $table) {
			$table->dropForeign('projects_customer_id_foreign');
		});
		Schema::table('rows', function(Blueprint $table) {
			$table->dropForeign('rows_estimate_id_foreign');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->dropForeign('modules_angle_id_foreign');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->dropForeign('modules_insulation_id_foreign');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->dropForeign('modules_finish_id_foreign');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->dropForeign('modules_frame_id_foreign');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->dropForeign('modules_nature_id_foreign');
		});
		Schema::table('examples', function(Blueprint $table) {
			$table->dropForeign('examples_range_id_foreign');
		});
		Schema::table('example_module', function(Blueprint $table) {
			$table->dropForeign('example_module_module_id_foreign');
		});
		Schema::table('example_module', function(Blueprint $table) {
			$table->dropForeign('example_module_example_id_foreign');
		});
		Schema::table('finishes', function(Blueprint $table) {
			$table->dropForeign('finishes_nature_id_foreign');
		});
		Schema::table('frames', function(Blueprint $table) {
			$table->dropForeign('frames_nature_id_foreign');
		});
		Schema::table('module_order', function(Blueprint $table) {
			$table->dropForeign('module_order_module_id_foreign');
		});
		Schema::table('module_order', function(Blueprint $table) {
			$table->dropForeign('module_order_order_id_foreign');
		});
		Schema::table('estimates', function(Blueprint $table) {
			$table->dropForeign('estimates_order_id_foreign');
		});
        Schema::table('estimates', function(Blueprint $table) {
            $table->dropForeign('estimates_project_id_foreign');
        });
        Schema::table('estimates', function(Blueprint $table) {
            $table->dropForeign('estimates_customer_id_foreign');
        });
	}
}