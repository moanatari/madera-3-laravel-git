<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModuleOrderTable extends Migration {

	public function up()
	{
		Schema::create('module_order', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->float('length')->nullable();
			$table->integer('stage')->nullable();
            $table->float('discount')->nullable()->default('0');
			$table->integer('module_id')->unsigned()->nullable();
			$table->integer('order_id')->unsigned()->nullable();
			$table->integer('parent_id')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('module_order');
	}
}