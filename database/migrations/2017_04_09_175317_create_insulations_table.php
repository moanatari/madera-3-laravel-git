<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInsulationsTable extends Migration {

	public function up()
	{
		Schema::create('insulations', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->float('price')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('insulations');
	}
}