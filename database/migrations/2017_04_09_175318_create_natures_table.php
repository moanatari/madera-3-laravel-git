<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNaturesTable extends Migration {

	public function up()
	{
		Schema::create('natures', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->string('characteristic')->nullable();
			$table->string('unit')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('natures');
	}
}