<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExamplesTable extends Migration {

	public function up()
	{
		Schema::create('examples', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->integer('range_id')->unsigned()->nullable();
		});
	}

	public function down()
	{
		Schema::drop('examples');
	}
}