<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFramesTable extends Migration {

	public function up()
	{
		Schema::create('frames', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->float('price')->nullable();
			$table->integer('nature_id')->unsigned()->nullable();
		});
	}

	public function down()
	{
		Schema::drop('frames');
	}
}