<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnglesTable extends Migration {

	public function up()
	{
		Schema::create('angles', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->float('price')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('angles');
	}
}