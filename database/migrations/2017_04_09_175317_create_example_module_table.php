<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExampleModuleTable extends Migration {

	public function up()
	{
		Schema::create('example_module', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->float('length')->nullable();
			$table->integer('stage')->nullable();
			$table->integer('module_id')->unsigned()->nullable();
			$table->integer('example_id')->unsigned()->nullable();
			$table->integer('parent_id')->unsigned()->nullable();
		});
	}

	public function down()
	{
		Schema::drop('example_module');
	}
}