<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstimatesTable extends Migration {

	public function up()
	{
		Schema::create('estimates', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('order_id')->unsigned()->nullable();
            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
			$table->float('discount')->nullable();
			$table->float('price')->nullable();
			$table->integer('days')->nullable()->default('30');
		});
	}

	public function down()
	{
		Schema::drop('estimates');
	}
}