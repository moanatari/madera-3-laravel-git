<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoversTable extends Migration {

	public function up()
	{
		Schema::create('covers', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->float('price')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('covers');
	}
}