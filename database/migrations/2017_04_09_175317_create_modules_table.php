<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModulesTable extends Migration {

	public function up()
	{
		Schema::create('modules', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->nullable();
			$table->float('price')->nullable();
			$table->string('coupePrincipe')->nullable();
			$table->string('cctp')->nullable();
			$table->integer('angle_id')->unsigned()->nullable();
			$table->integer('insulation_id')->unsigned()->nullable();
			$table->integer('finish_id')->unsigned()->nullable();
			$table->integer('frame_id')->unsigned()->nullable();
			$table->integer('nature_id')->unsigned()->nullable();
		});
	}

	public function down()
	{
		Schema::drop('modules');
	}
}