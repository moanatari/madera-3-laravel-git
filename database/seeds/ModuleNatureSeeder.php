<?php

use Illuminate\Database\Seeder;

class ModuleNatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('natures')->insert([
            'id' => 1,
            'name' => 'Mur Extérieur',
            'characteristic' => 'Hauteur-Longueur',
            'unit' => 'M linéaire',
        ]);

        DB::table('natures')->insert([
            'id' => 2,
            'name' => 'Cloison intérieur',
            'characteristic' => 'Hauteur-Longueur',
            'unit' => 'M linéaire',
        ]);

        DB::table('natures')->insert([
            'id' => 3,
            'name' => 'Fenêtre',
            'characteristic' => null,
            'unit' => 'Unité',
        ]);

        DB::table('natures')->insert([
            'id' => 4,
            'name' => 'Escalier',
            'characteristic' => '',
            'unit' => 'Unité',
        ]);

        DB::table('natures')->insert([
            'id' => 5,
            'name' => 'Porte',
            'characteristic' => '',
            'unit' => 'Unité',
        ]);

        DB::table('natures')->insert([
            'id' => 6,
            'name' => 'Plancher sur dalle',
            'characteristic' => 'Hauteur-Longueur',
            'unit' => 'M 2',
        ]);

        DB::table('natures')->insert([
            'id' => 7,
            'name' => 'Plancher porteur',
            'characteristic' => 'Hauteur-Longueur',
            'unit' => 'M 2',
        ]);

        DB::table('natures')->insert([
            'id' => 8,
            'name' => 'Couverture',
            'characteristic' => 'Hauteur-Longueur',
            'unit' => 'M 2',
        ]);

    }
}
