<?php

use Illuminate\Database\Seeder;

class FinishesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('finishes')->insert([
            'id' => 1,
            'name' => 'Mur extérieur Bois',
            'price' => 20,
            'nature_id' => 1,
        ]);

        DB::table('finishes')->insert([
            'id' => 2,
            'name' => ' Cloison interieur Bois',
            'price' => 20,
            'nature_id' => 2,
        ]);

        DB::table('finishes')->insert([
            'id' => 3,
            'name' => 'Fenetre Bois',
            'price' => 20,
            'nature_id' => 3,
        ]);

        DB::table('finishes')->insert([
            'id' => 4,
            'name' => 'Porte Bois',
            'price' => 20,
            'nature_id' => 5,
        ]);

        DB::table('finishes')->insert([
            'id' => 5,
            'name' => 'Esclalier Bois',
            'price' => 25,
            'nature_id' => 4,
        ]);

//        DB::table('finishes')->insert([
//            'id' => 6,
//            'name' => 'Couverture ardoise',
//            'price' => 25,
//            'nature_id' => 8,
//        ]);

        DB::table('finishes')->insert([
            'id' => 7,
            'name' => 'Plancher bois sur dalle',
            'price' => 10,
            'nature_id' => 6,
        ]);

        DB::table('finishes')->insert([
            'id' => 8,
            'name' => 'Plancher bois porteur',
            'price' => 20,
            'nature_id' => 7,
        ]);

        DB::table('finishes')->insert([
            'id' => 9,
            'name' => 'Mur extérieur Crépis',
            'price' => 10,
            'nature_id' => 1,
        ]);

        DB::table('finishes')->insert([
            'id' => 10,
            'name' => ' Cloison interieur Crépis',
            'price' => 10,
            'nature_id' => 2,
        ]);

        DB::table('finishes')->insert([
            'id' => 11,
            'name' => 'Fenetre Plastique',
            'price' => 10,
            'nature_id' => 3,
        ]);

        DB::table('finishes')->insert([
            'id' => 12,
            'name' => 'Porte Plastique',
            'price' => 10,
            'nature_id' => 5,
        ]);

        DB::table('finishes')->insert([
            'id' => 13,
            'name' => 'Plancher Lino sur dalle',
            'price' => 5,
            'nature_id' => 6,
        ]);

        DB::table('finishes')->insert([
            'id' => 14,
            'name' => 'Plancher Lino porteur',
            'price' => 15,
            'nature_id' => 7,
        ]);
    }
}
