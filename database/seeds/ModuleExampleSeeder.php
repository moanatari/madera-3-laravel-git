<?php

use Illuminate\Database\Seeder;

class ModuleExampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('example_module')->insert([
            'id' => 1,
            'name' => 'Plancher RDC Bois',
            'length' => 300,
            'module_id' => 8,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 2,
            'name' => 'Mur Nord - 1 - Bois - Laine de verre - Angle entrant',
            'length' => 10,
            'module_id' => 1,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 3,
            'name' => 'Mur Est - 1 - Bois - Laine de verre - Angle sortant',
            'length' => 10,
            'module_id' => 2,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 4,
            'name' => 'Mur Nord - 2 - Bois - laine de verre - Angle entrant',
            'length' => 10,
            'module_id' => 1,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 5,
            'name' => 'Mur Est - 2 - Bois - Laine de verre - ange entrant',
            'length' => 10,
            'module_id' => 1,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 6,
            'name' => 'Mur Sud - 1 Bois - Laine de verre - Angle entrant',
            'length' => 20,
            'module_id' => 1,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 7,
            'name' => 'Mur Ouest 1 - Bois - Laine de verrre - Angle entrant',
            'length' => 20,
            'module_id' => 1,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 8,
            'name' => 'Cloison Nord - 1 - Bois',
            'length' => 10,
            'module_id' => 3,
            'example_id' => 1,
            'parent_id' => 2,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 9,
            'name' => 'Cloison Est - 1 - Bois',
            'length' => 10,
            'module_id' => 3,
            'example_id' => 1,
            'parent_id' => 3,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 10,
            'name' => 'Cloison Nord - 2 - Bois',
            'length' => 10,
            'module_id' => 3,
            'example_id' => 1,
            'parent_id' => 4,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 11,
            'name' => 'Cloison Est - 2 - Bois',
            'length' => 10,
            'module_id' => 3,
            'example_id' => 1,
            'parent_id' => 5,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 12,
            'name' => 'Cloison Sud - 1 - Bois',
            'length' => 20,
            'module_id' => 3,
            'example_id' => 1,
            'parent_id' => 6,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 13,
            'name' => 'Cloison Ouest 1 - Bois',
            'length' => 20,
            'module_id' => 3,
            'example_id' => 1,
            'parent_id' => 7,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 14,
            'name' => 'Fenêtre Est - 1 - Bois',
            'length' => 1,
            'module_id' => 4,
            'example_id' => 1,
            'parent_id' => 5,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 15,
            'name' => 'Fenêtre Sud - 1 - Bois',
            'length' => 1,
            'module_id' => 4,
            'example_id' => 1,
            'parent_id' => 6,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 16,
            'name' => 'Fenêtre Sud - 2 - Bois',
            'length' => 1,
            'module_id' => 4,
            'example_id' => 1,
            'parent_id' => 6,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 17,
            'name' => 'Fenêtre Ouest - 1 - Bois',
            'length' => 1,
            'module_id' => 4,
            'example_id' => 1,
            'parent_id' => 7,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 18,
            'name' => 'Escalier',
            'length' => 1,
            'module_id' => 6,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 19,
            'name' => 'Porte Est - 1 - Bois',
            'length' => 1,
            'module_id' => 5,
            'example_id' => 1,
            'parent_id' => 3,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 20,
            'name' => 'Toit couverture ardoise RDC',
            'length' => 100,
            'module_id' => 7,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 21,
            'name' => 'Plancher 1er Bois',
            'length' => 200,
            'module_id' => 9,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 22,
            'name' => 'Mur Nord - 1 - Bois - Laine de verre - Angle entrant',
            'length' => 10,
            'module_id' => 1,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 23,
            'name' => 'Mur Est - 1 - Bois - Laine de verre - Angle entrant',
            'length' => 20,
            'module_id' => 1,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 24,
            'name' => 'Mur Sud - 1 - Bois - Laine de verre - Angle entrant',
            'length' => 10,
            'module_id' => 1,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 25,
            'name' => 'Mur Ouest - 1 - Bois - Laine de verre - Angle entrant',
            'length' => 20,
            'module_id' => 1,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 26,
            'name' => 'Cloison Nord - 1 - Bois',
            'length' => 10,
            'module_id' => 3,
            'example_id' => 1,
            'parent_id' => 22,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 27,
            'name' => 'Cloison Est - 1 - Bois',
            'length' => 20,
            'module_id' => 3,
            'example_id' => 1,
            'parent_id' => 23,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 28,
            'name' => 'Cloison Sud - 1 - Bois',
            'length' => 10,
            'module_id' => 3,
            'example_id' => 1,
            'parent_id' => 24,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 29,
            'name' => 'Cloison Ouest 1 - Bois',
            'length' => 20,
            'module_id' => 3,
            'example_id' => 1,
            'parent_id' => 25,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 30,
            'name' => 'Fenêtre Est - 1 - Bois',
            'length' => 1,
            'module_id' => 4,
            'example_id' => 1,
            'parent_id' => 23,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 31,
            'name' => 'Fenêtre Est - 2 - Bois',
            'length' => 1,
            'module_id' => 4,
            'example_id' => 1,
            'parent_id' => 23,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 32,
            'name' => 'Fenêtre Sud - 1 - Bois',
            'length' => 1,
            'module_id' => 4,
            'example_id' => 1,
            'parent_id' => 24,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 33,
            'name' => 'Fenêtre Ouest - 1 - Bois',
            'length' => 1,
            'module_id' => 4,
            'example_id' => 1,
            'parent_id' => 25,
            'stage' => 1,
        ]);

        DB::table('example_module')->insert([
            'id' => 34,
            'name' => 'Toit couverture ardoise 1er',
            'length' => 200,
            'module_id' => 7,
            'example_id' => 1,
            'parent_id' => null,
            'stage' => 1,
        ]);


    }
}
