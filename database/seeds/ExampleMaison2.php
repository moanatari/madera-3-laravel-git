<?php

use Illuminate\Database\Seeder;

class ExampleMaison2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('example_module')->insert([
            'id' => 35,
            'name' => 'Plancher RDC Lino',
            'length' => 400,
            'module_id' => 20,
            'example_id' => 2,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 36,
            'name' => 'Mur Nord - 1 - Crépis - Laine de verre - Angle entrant',
            'length' => 20,
            'module_id' => 10,
            'example_id' => 2,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 37,
            'name' => 'Mur Est - 1 - Crépis - Laine de verre - Angle entrant',
            'length' => 20,
            'module_id' => 10,
            'example_id' => 2,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 38,
            'name' => 'Mur Sud - 1 - Crépis - Laine de verre - Angle entrant',
            'length' => 20,
            'module_id' => 10,
            'example_id' => 2,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 39,
            'name' => 'Mur Ouest - 1 - Crépis - Laine de verre - Angle entrant',
            'length' => 20,
            'module_id' => 10,
            'example_id' => 2,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 40,
            'name' => 'Porte sud plastique',
            'length' => 1,
            'module_id' => 17,
            'example_id' => 2,
            'parent_id' => 4,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 50,
            'name' => 'Fenetre 1 Sud plastique',
            'length' => 1,
            'module_id' => 13,
            'example_id' => 2,
            'parent_id' => 4,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 51,
            'name' => 'Fenetre 2 Sud plastique',
            'length' => 1,
            'module_id' => 13,
            'example_id' => 2,
            'parent_id' => 4,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 52,
            'name' => 'Fenetre 1 Ouest plastique',
            'length' => 1,
            'module_id' => 13,
            'example_id' => 2,
            'parent_id' => 5,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 53,
            'name' => 'Fenetre 1 Est plastique',
            'length' => 1,
            'module_id' => 13,
            'example_id' => 2,
            'parent_id' => 3,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 54,
            'name' => 'Couverture tuiles',
            'length' => 400,
            'module_id' => 19,
            'example_id' => 2,
            'parent_id' => null,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 55,
            'name' => 'Cloison intérieur crépis Nord',
            'length' => 20,
            'module_id' => 12,
            'example_id' => 2,
            'parent_id' => 36,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 56,
            'name' => 'Cloison intérieur crépis Est',
            'length' => 20,
            'module_id' => 12,
            'example_id' => 2,
            'parent_id' => 37,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 57,
            'name' => 'Cloison intérieur crépis Sud',
            'length' => 20,
            'module_id' => 12,
            'example_id' => 2,
            'parent_id' => 38,
            'stage' => 0,
        ]);

        DB::table('example_module')->insert([
            'id' => 58,
            'name' => 'Cloison intérieur crépis Nord',
            'length' => 20,
            'module_id' => 12,
            'example_id' => 2,
            'parent_id' => 39,
            'stage' => 0,
        ]);
    }
}
