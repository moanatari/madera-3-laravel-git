<?php

use Illuminate\Database\Seeder;

class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ranges')->insert([
            'id' => 1,
            'name' => 'Luxe',
        ]);

        DB::table('ranges')->insert([
            'id' => 2,
            'name' => 'Standard',
        ]);

        DB::table('examples')->insert([
            'id' => 1,
            'name' => 'Maison bois 1 avec un étage',
            'range_id' => 1,
        ]);

        DB::table('examples')->insert([
            'id' => 2,
            'name' => 'Maison crépis',
            'range_id' => 2,
        ]);

        DB::table('examples')->insert([
            'id' => 3,
            'name' => 'Commande vierge',
            'range_id' => 2,
        ]);

//        DB::table('modules')->insert([
//            'id' => 1,
//            'name' => 'luxe',
//        ]);

        DB::table('insulations')->insert([
            'id' => 1,
            'name' => 'Laine de verre',
            'price' => 10,
        ]);

        DB::table('insulations')->insert([
            'id' => 2,
            'name' => 'Isolant mince aluminium',
            'price' => 30,
        ]);

        DB::table('angles')->insert([
            'id' => 1,
            'name' => 'Angle entrant',
            'price' => 10,
        ]);

        DB::table('angles')->insert([
            'id' => 2,
            'name' => 'Angle sortant',
            'price' => 15,
        ]);

        DB::table('covers')->insert([
            'id' => 1,
            'name' => 'Ardoise',
            'price' => 20,
        ]);

        DB::table('covers')->insert([
            'id' => 2,
            'name' => 'Tuiles',
            'price' => 30,
        ]);























        DB::table('frames')->insert([
            'id' => 1,
            'name' => 'fenetre frame PVC',
            'price' => 20,
        ]);

        DB::table('frames')->insert([
            'id' => 2,
            'name' => 'porte frame Metal',
            'price' => 30,
        ]);

        DB::table('frames')->insert([
            'id' => 3,
            'name' => 'fenetre frame metal',
            'price' => 30,
        ]);

        DB::table('frames')->insert([
            'id' => 4,
            'name' => 'porte frame Plastique',
            'price' => 20,
        ]);



        DB::table('modules')->insert([
            'id' => 1,
            'name' => 'Mur exterieur bois angle entrant laine de verre',
            'price' => 50,
            'coupePrincipe' => '',
            'cctp' => '',
            'angle_id' => 1,
            'insulation_id' => 1,
            'finish_id' => 1,
            'frame_id' => null,
            'nature_id' => 1,
        ]);

        DB::table('modules')->insert([
            'id' => 2,
            'name' => 'Mur exterieur bois angle sortant laine de verre',
            'price' => 50,
            'coupePrincipe' => '',
            'cctp' => '',
            'angle_id' => 2,
            'insulation_id' => 1,
            'finish_id' => 1,
            'frame_id' => null,
            'nature_id' => 1,
        ]);

        DB::table('modules')->insert([
            'id' => 3,
            'name' => 'Cloison interieur Bois',
            'price' => 25,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 2,
            'frame_id' => null,
            'nature_id' => 2,
        ]);

        DB::table('modules')->insert([
            'id' => 4,
            'name' => 'Fenetre bois frame pvc',
            'price' => 60,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 3,
            'frame_id' => 1,
            'nature_id' => 3,
        ]);

        DB::table('modules')->insert([
            'id' => 5,
            'name' => 'Porte bois frame metal',
            'price' => 70,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 4,
            'frame_id' => 2,
            'nature_id' => 5,
        ]);

        DB::table('modules')->insert([
            'id' => 6,
            'name' => 'Escalier bois',
            'price' => 125,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 5,
            'frame_id' => null,
            'nature_id' => 4,
        ]);

        DB::table('modules')->insert([
            'id' => 7,
            'name' => 'Couverture ardoise',
            'price' => 60,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => null,
            'frame_id' => null,
            'nature_id' => 8,
        ]);

        DB::table('modules')->insert([
            'id' => 8,
            'name' => 'Plancher bois sur dalle',
            'price' => 30,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 7,
            'frame_id' => null,
            'nature_id' => 6,
        ]);

        DB::table('modules')->insert([
            'id' => 9,
            'name' => 'Plancher bois porteur',
            'price' => 50,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 8,
            'frame_id' => null,
            'nature_id' => 7,
        ]);

        DB::table('modules')->insert([
            'id' => 10,
            'name' => 'Mur exterieur crépis angle entrant laine de verre',
            'price' => 50,
            'coupePrincipe' => '',
            'cctp' => '',
            'angle_id' => 1,
            'insulation_id' => 1,
            'finish_id' => 9,
            'frame_id' => null,
            'nature_id' => 1,
        ]);

        DB::table('modules')->insert([
            'id' => 11,
            'name' => 'Mur exterieur Crépis angle sortant laine de verre',
            'price' => 50,
            'coupePrincipe' => '',
            'cctp' => '',
            'angle_id' => 2,
            'insulation_id' => 1,
            'finish_id' => 9,
            'frame_id' => null,
            'nature_id' => 1,
        ]);

        DB::table('modules')->insert([
            'id' => 12,
            'name' => 'Cloison interieur Crépis',
            'price' => 25,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 10,
            'frame_id' => null,
            'nature_id' => 2,
        ]);

        DB::table('modules')->insert([
            'id' => 13,
            'name' => 'Fenetre plastique frame pvc',
            'price' => 60,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 11,
            'frame_id' => 1,
            'nature_id' => 3,
        ]);

        DB::table('modules')->insert([
            'id' => 14,
            'name' => 'Fenetre plastique frame metal',
            'price' => 60,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 11,
            'frame_id' => 3,
            'nature_id' => 3,
        ]);

        DB::table('modules')->insert([
            'id' => 15,
            'name' => 'Fenetre bois frame metal',
            'price' => 60,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 3,
            'frame_id' => 3,
            'nature_id' => 3,
        ]);

        DB::table('modules')->insert([
            'id' => 16,
            'name' => 'Porte bois frame plastique',
            'price' => 70,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 4,
            'frame_id' => 4,
            'nature_id' => 5,
        ]);

        DB::table('modules')->insert([
            'id' => 17,
            'name' => 'Porte plastique frame plastique',
            'price' => 70,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 12,
            'frame_id' => 4,
            'nature_id' => 5,
        ]);

        DB::table('modules')->insert([
            'id' => 18,
            'name' => 'Porte plastique frame metal',
            'price' => 70,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 12,
            'frame_id' => 2,
            'nature_id' => 5,
        ]);

        DB::table('modules')->insert([
            'id' => 19,
            'name' => 'Couverture tuiles',
            'price' => 40,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => null,
            'frame_id' => null,
            'nature_id' => 8,
        ]);

        DB::table('modules')->insert([
            'id' => 20,
            'name' => 'Plancher lino sur dalle',
            'price' => 30,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 13,
            'frame_id' => null,
            'nature_id' => 6,
        ]);

        DB::table('modules')->insert([
            'id' => 21,
            'name' => 'Plancher lino porteur',
            'price' => 50,
            'coupePrincipe' => '',
            'cctp' => '',
            'finish_id' => 14,
            'frame_id' => null,
            'nature_id' => 7,
        ]);

        DB::table('modules')->insert([
            'id' => 22,
            'name' => 'Mur exterieur crépis angle entrant isolant aluminium',
            'price' => 50,
            'coupePrincipe' => '',
            'cctp' => '',
            'angle_id' => 1,
            'insulation_id' => 2,
            'finish_id' => 9,
            'frame_id' => null,
            'nature_id' => 1,
        ]);

        DB::table('modules')->insert([
            'id' => 23,
            'name' => 'Mur exterieur Crépis angle sortant isolant aluminium',
            'price' => 50,
            'coupePrincipe' => '',
            'cctp' => '',
            'angle_id' => 2,
            'insulation_id' => 2,
            'finish_id' => 9,
            'frame_id' => null,
            'nature_id' => 1,
        ]);

        DB::table('modules')->insert([
            'id' => 24,
            'name' => 'Mur exterieur Bois angle entrant isolant aluminium',
            'price' => 50,
            'coupePrincipe' => '',
            'cctp' => '',
            'angle_id' => 1,
            'insulation_id' => 2,
            'finish_id' => 1,
            'frame_id' => null,
            'nature_id' => 1,
        ]);

        DB::table('modules')->insert([
            'id' => 25,
            'name' => 'Mur exterieur Bois angle sortant isolant aluminium',
            'price' => 50,
            'coupePrincipe' => '',
            'cctp' => '',
            'angle_id' => 2,
            'insulation_id' => 2,
            'finish_id' => 1,
            'frame_id' => null,
            'nature_id' => 1,
        ]);

    }
}
