<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
		$this->call(UsersTableSeeder::class);
        $this->call(ModuleNatureSeeder::class);
        $this->call(FinishesSeeder::class);
        $this->call(ModulesSeeder::class);
        $this->call(ModuleExampleSeeder::class);
        $this->call(ExampleMaison2::class);
    }
}
