# Conception de devis avec Laravel 5.3  

Application de création de devis pour maison modulaire développé avec Laravel.  

## Pré-requis  

- php  
- composer
- mysql



## Installation  
  
- Créer une base de données pour le projet ( exemple : 'madera-laravel')  
  
- Cloner le projet  
`git clone https://moanatari@gitlab.com/moanatari/madera-3-laravel-git.git`   
  
- Aller dans le dossier du projet  
`cd madera-3-laravel-git`  
  
- Copier le fichier de configuration  
`cp .env.example .env`  
  
- Installer le projet   
`composer install`  
   
- Générer le token de l'application   
`php artisan key:generate`  
  
- Indiquer les informations de connexion à la base de données en modifiant le fichier '.env' exemple :  
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=madera-laravel
DB_USERNAME=root
DB_PASSWORD=root
```   
  
- Inserer les tables et les données  
`php artisan migrate --seed`  
  
- Lancer le projet  
`php artisan serve`  
  
Maintenant le projet est accéssible à l'adresse : `http://127.0.0.1:8000/home`   
  

## Identifiant  
- login : `commercial@madera.fr`  
- password : `admin`  

