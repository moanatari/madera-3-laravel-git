<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
    //return view('welcome');
});

Auth::routes();

Route::get('/home', 'FirstController@index')->name('home');

Route::get('/project/createcustomer/{id}', 'ProjectController@createCustomer')->name('project.createCustomer');
Route::post('/project/createcustomer', 'ProjectController@storeCustomer')->name('project.storeCustomer');
Route::get('/project/selectcustomer/{id}', 'ProjectController@selectCustomer')->name('project.selectCustomer');
Route::post('/project/selectcustomer', 'ProjectController@storeSelectedCustomer')->name('project.storeSelectedCustomer');

Route::get('/order/selectexample/{id}', 'OrderController@selectExample')->name('order.selectExample');
Route::post('/order/selectexample', 'OrderController@storeSelectedExample')->name('order.storeSelectedExample');



Route::get('/order/selectnature/{order_id}', 'OrderController@selectNature')->name('order.selectNature');
Route::post('/order/createmodule', 'OrderController@passCreateModule')->name('order.passCreateModule');
Route::get('/order/createmodule/{order_id}/{nature_id}', 'OrderController@createModule')->name('order.createModule');
Route::post('/order/storecreatedmodule', 'OrderController@storeCreatedModule')->name('order.storeCreatedModule');
Route::get('/order/editmodule/{id}', 'OrderController@editModule')->name('order.editModule');
Route::post('/order/updateEditedModule', 'OrderController@updateEditedModule')->name('order.updateEditedModule');
Route::get('/order/confirmdeletemodule/{id}', 'OrderController@confirmDeleteModule')->name('order.confirmDeleteModule');
Route::get('/order/deletemodule/{id}', 'OrderController@deleteModule')->name('order.deleteModule');


Route::get('/estimate/validateestimate/{id}', 'EstimateController@validateEstimate')->name('estimate.validateEstimate');
Route::post('/estimate/validateestimate', 'EstimateController@storeEstimate')->name('estimate.storeEstimate');

Route::resource('order', 'OrderController');
Route::resource('estimate', 'EstimateController');
Route::resource('project', 'ProjectController');
Route::resource('customer', 'CustomerController');
Route::resource('row', 'RowController');
Route::resource('module', 'ModuleController');
Route::resource('range', 'RangeController');
Route::resource('example', 'ExampleController');
Route::resource('moduleexample', 'ModuleExampleController');
Route::resource('angle', 'AngleController');
Route::resource('insulation', 'InsulationController');
Route::resource('finish', 'FinishController');
Route::resource('cover', 'CoverController');
Route::resource('frame', 'FrameController');
Route::resource('nature', 'NatureController');
Route::resource('moduleorder', 'ModuleOrderController');