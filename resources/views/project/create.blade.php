@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    Nouveau projet
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Formulaire de création d'un projet</div>

                    <div class="panel-body">

                        <form action="{{route('project.store')}}" method="POST">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="">Nom du projet</label>
                                <input type="text" name="name" class="form-control" @if(!old('name'))value="Projet n° {{$count}}" @else value="{{old('name')}}" @endif required>
                            </div>
                            <div class="form-group">
                                <label for="">Référence projet</label>
                                <input type="text" name="reference" class="form-control" value="{{ old('reference') }}">
                            </div>

                            <div class="col-md-12 text-center">
                                <a href="{{route('home')}}" class="btn btn-lg btn-default"> <i class=" fa fa-undo" aria-hidden="true"></i> Annuler</a>
                                <button type="submit" class="btn btn-lg btn-success">Créer le projet</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
