@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    Projet
                </p>
                <div class="panel panel-default">
                    <div class="panel-heading">{{$project->name}}</div>

                    <div class="panel-body">
                        <h2>Information sur le projet {{$project->name}}</h2>
                        <p>Numéro : <strong>{{$project->id}}</strong> </p>
                        <p>Nom : <strong>{{$project->name}}</strong></p>
                        <p>Référence : <strong>{{$project->reference}}</strong></p>

                        <p><a href="{{route('project.edit', $project->id)}}" class="btn btn-default"><i class="fa fa-pencil" aria-hidden="true"></i> Modifier les informations</a></p>



                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Client</div>

                    <div class="panel-body">
                        @if($project->customer_id)
                            <p><strong>{{$project->customer->name}} {{$project->customer->firstname}}</strong></p>
                            <label>Téléphone</label>
                            <p>{{$project->customer->phone}}</p>
                            <label>E-Mail</label>
                            <p>{{$project->customer->mail}}</p>
                            <label>Adresse</label>
                            <p>{{$project->customer->address}} <br> {{$project->customer->postalCode}} {{$project->customer->city}}</p>
                            <p><a href="{{route('customer.show', $project->customer->id)}}" class="btn btn-default"><i class="fa fa-user" aria-hidden="true"></i> Voir le client</a></p>
                        @else
                            Associer un client : <br>
                            <a href="{{route('project.createCustomer', $project->id)}}" class="btn btn-default"><i class="fa fa-user-o" aria-hidden="true"></i> Créer un client pour ce projet</a><br>
                            <br>
                            <a href="{{route('project.selectCustomer', $project->id)}}" class="btn btn-default"><i class="fa fa-user" aria-hidden="true"></i> Ajouter un client existant</a><br><br>
                        @endif
                </div>


                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Commandes en cours</div>

                    <div class="panel-body">

                        <a href="{{route('order.selectExample', $project->id)}}" class="btn btn-default"><i class="fa fa-file" aria-hidden="true"></i> Créer une commande</a><br><br>

                        @if($orders->count())
                            <table class="table table-bordered">
                                <tr>
                                    <th>Numéro</th>
                                    <th>Nb modules</th>
                                    <th>Estimation</th>

                                    <th>Date</th>

                                    <th></th>
                                </tr>
                                @foreach($orders as $order)

                                        <tr>
                                            <td><a href="{{route('order.show', $order->id)}}">
                                                    Numéro {{$order->id}}
                                                </a></td>

                                            <td>{{$order->modules->count()}}</td>
                                            <td>{{$order->getTotalPrice()}} €</td>
                                            <td>{{date('d / m / Y', strtotime($order->created_at))}}</td>

                                            <td><a href="{{route('order.show', $order->id)}}" class="btn btn-default">
                                                    <i class="fa fa-file" aria-hidden="true"></i> Voir la commande
                                                </a></td>
                                        </tr>

                                @endforeach
                            </table>
                        @else
                            Aucune commande.
                        @endif
                    </div>


                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Devis de ce projet</div>

                    <div class="panel-body">

                        @if($estimates->count())
                            <table class="table table-bordered">
                                <tr>
                                    <th>Numéro devis</th>
                                    <th>Nombre de modules</th>
                                    <th>Prix total</th>
                                    <th>Date</th>
                                    <th>Nb jour validité</th>
                                    <th></th>
                                </tr>
                                @foreach($estimates as $estimate)
                                    <tr>
                                        <td><a href="{{route('estimate.show', $estimate->id)}}">Devis n° {{$estimate->id}}</a></td>
                                        <td>{{$estimate->rows->count()}}</td>
                                        <td>{{$estimate->price}} €</td>
                                        <td>{{$estimate->created_at}}</td>
                                        <td>{{$estimate->days}}</td>
                                        <td><a href="{{route('estimate.show', $estimate->id)}}" class="btn btn-default"><i class="fa fa-file" aria-hidden="true"></i> Voir le devis</a></td>
                                    </tr>
                                @endforeach
                            </table>
                            @else
                        Aucun devis pour ce projet
                            @endif

                    </div>


                </div>



            </div>
        </div>
    </div>
@endsection
