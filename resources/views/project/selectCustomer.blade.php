@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('project.show',$project_id)}}">Projet</a>
                    /
                    Sélection d'un client
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Choisir un client pour le projet</div>

                    <div class="panel-body">

                        @if($customers->count())
                            <table class="table table-bordered">
                                <tr>
                                    <th>Nom</th>
                                    <th>Prenom</th>
                                    <th>Ville</th>
                                    <th>Selection</th>
                                </tr>
                                @foreach($customers as $customer)
                                    <tr>
                                        <td>{{$customer->name}}</td>
                                        <td>{{$customer->firstname}}</td>
                                        <td>{{$customer->city}}</td>
                                        <td>
                                            <form action="{{route('project.storeSelectedCustomer')}}" method="post">
                                                {{ csrf_field() }}
                                                <input value="{{$customer->id}}" name="customer_id" type="text" hidden required>
                                                <input value="{{$project_id}}" name="project_id" type="text" hidden required>
                                                <button type="submit" class="btn btn-default"><i class="fa fa-user" aria-hidden="true"></i> Choisir ce client</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            @else
                            <p><strong>Aucun client dans la base de données</strong></p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
