@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    Mise à jour du projet
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Formulaire de modification d'un projet</div>

                    <div class="panel-body">

                        <form action="{{route('project.update', $project->id)}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <div class="form-group">
                                <label for="">Nom du projet</label>
                                <input type="text" name="name" class="form-control" @if(!old('name')) value="{{$project->name}}" @else value="{{ old('name') }}" @endif required>
                            </div>
                            <div class="form-group">
                                <label for="">Référence projet</label>
                                <input type="text" name="reference" class="form-control" @if(!old('reference')) value="{{$project->reference}}" @else value="{{old('reference')}}" @endif>
                            </div>

                            <div class="col-md-12 text-center">
                                <a href="{{route('project.show', $project->id)}}" class="btn btn-lg btn-default"><i class="fa fa-undo" aria-hidden="true"></i> Annuler</a>
                                <button type="submit" class="btn btn-success btn-lg"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Modifier les informations du projet</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
