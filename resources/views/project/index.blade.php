@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    Projets
                </p>
                <div class="panel panel-default">

                    <div class="panel-heading">Liste des derniers projets</div>

                    <div class="panel-body">
                        <div class="col-md-12 ">
                            <p><a class="btn btn-default " href="{{route('project.create')}}"><i class="fa fa-home" aria-hidden="true"></i> Créer un nouveau projet</a></p>
                        </div>

                        @if($projects->count())
                            <table class="table-bordered table">
                                <tr>
                                    <th>Nom du projet</th>
                                    <th>Référence</th>
                                    <th>Client</th>
                                    <th>Date</th>
                                    <th></th>
                                </tr>
                                @foreach($projects as $project)
                                    <tr>
                                        <td><a href="{{route('project.show',$project->id)}}">{{$project->name}}</a></td>
                                        <td>{{$project->reference}}</td>
                                        <td>@if($project->customer_id != null)
                                                {{$project->customer->name}} {{$project->customer->firstname}}
                                            @else
                                                Pas de client
                                            @endif </td>
                                        <td>{{date('d / m / Y', strtotime($project->created_at))}}</td>
                                        <td><a href="{{route('project.show', $project->id)}}" class="btn btn-default"><i class="fa fa-file" aria-hidden="true"></i> Voir le projet</a></td>

                                    </tr>
                                @endforeach
                            </table>
                            @else
                        <p><strong>Aucun projet dans la base de données</strong></p>
                            @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
