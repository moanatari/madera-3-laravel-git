@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('order.show',$order_id)}}">Commande</a>
                    /
                    Création d'un devis
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Création du devis pour une commande</div>

                    <div class="panel-body">

                        <form action="{{route('estimate.storeEstimate')}}" method="post">

                            {{ csrf_field() }}
                            <input type="text" name="order_id" hidden value="{{$order_id}}">

                            <div class="form-group">
                                <label>Nombre de jour de validité du devis</label>
                                <input type="text" class="form-control" name="days" required @if(!old('days')) value="30" @else value="{{old('days')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label>Remise</label>
                                <input type="text" class="form-control" name="discount" required @if(!old('discount')) value="0" @else value="{{old('discount')}}" @endif>
                            </div>

                            <div class="col-md-12 text-center"><button type="submit" class="btn btn-success btn-lg"><i class="fa fa-file" aria-hidden="true"></i> Créer le devis</button></div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
