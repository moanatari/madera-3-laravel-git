@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('order.show',$estimate->order_id)}}">Commande</a>
                    /
                    Devis {{$estimate->id}}
                </p>
                <div class="panel panel-default">
                    <div class="panel-heading">Devis numéro {{$estimate->id}}</div>

                    <div class="panel-body">
                        <p>Date : {{date('d / m / Y', strtotime($estimate->created_at))}}</p>
                        @if($estimate->discount != 0)
                            <p>Remise : {{$estimate->discount}}%</p>
                        @endif
                        <p>Ce devis est vallable pendant <strong>{{$estimate->days}}</strong> jours.</p>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Client</div>

                    <div class="panel-body">

                        <p><strong>{{$customer->name}} {{$customer->firstname}}</strong></p>
                        <label>Téléphone</label>
                        <p>{{$customer->phone}}</p>
                        <label>E-Mail</label>
                        <p>{{$customer->mail}}</p>
                        <label>Adresse</label>
                        <p>{{$customer->address}} <br> {{$customer->postalCode}} {{$customer->city}}</p>
                        <p><a href="{{route('customer.show', $customer->id)}}" class="btn btn-default"><i class="fa fa-user" aria-hidden="true"></i> Voir le client</a></p>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Liste des modules :</div>

                    <div class="panel-body">


                        <table class="table table-bordered">
                            <tr>
                                <th>Etage</th>
                                <th>Description du module</th>
                                <th>Nature du module</th>
                                <th>Qte</th>
                                <th>Unité</th>
                                <th>Prix U</th>
                                <th>Total</th>

                            </tr>

                            @foreach($estimate->rows as $row)
                                <tr>
                                    <td>@if($row->stage == 0)RDC @else {{$row->stage}} @endif</td>
                                    <td>{{$row->description}}</td>
                                    <td>{{$row->nature}}</td>
                                    <td>{{$row->quantity}}</td>
                                    <td>{{$row->natureUnit}}</td>
                                    <td>{{$row->unitPrice}} €</td>
                                    <td>@if($row->discount != 0)<strong>*</strong>@endif{{$row->price}} €</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="2"><strong>TOTAL :</strong></td>
                                <td colspan="2"><strong>@if($estimate->discount != 0)<strong>*</strong>@endif{{$estimate->price}} €</strong></td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
