@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('customer.index')}}">Clients</a>
                    /
                    Client {{$customer->id}}
                </p>
                <div class="panel panel-default">
                    <div class="panel-heading">{{$customer->name}} {{$customer->firstname}}</div>

                    <div class="panel-body">

                        <label>Téléphone</label>
                        <p>{{$customer->phone}}</p>
                        <label>E-Mail</label>
                        <p>{{$customer->mail}}</p>
                        <label>Adresse</label>
                        <p>{{$customer->address}} <br> {{$customer->postalCode}} {{$customer->city}}</p>

                        <p><a href="{{route('customer.edit', $customer->id)}}" class="btn btn-default"><i class="fa fa-pencil" aria-hidden="true"></i> Modifier le client</a></p>


                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Projets du client</div>

                    <div class="panel-body">



                        @if($customer->projects->count())
                            <table class="table table-bordered">
                                <tr>
                                    <th>Nom du projet</th>
                                    <th>Reférence du projet</th>
                                    <th>Date</th>
                                    <th></th>
                                </tr>

                                @foreach($customer->projects as $project)
                                    <tr>
                                        <td><a href="{{route('project.show', $project->id)}}">{{$project->name}}</a></td>
                                        <td>{{$project->reference}}</td>
                                        <td>{{date('d / m / Y', strtotime($project->created_at))}}</td>
                                        <td><a href="{{route('project.show', $project->id)}}" class="btn btn-default"><i class="fa fa-file" aria-hidden="true"></i> Voir le projet</a></td>

                                    </tr>
                                @endforeach
                            </table>
                        @else
                            <strong>Le client n'a pas de projet</strong>
                        @endif

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Devis du client</div>

                    <div class="panel-body">



                        @if($estimates->count())
                            <table class="table table-bordered">
                                <tr>
                                    <th>Numéro devis</th>
                                    <th>Nombre de modules</th>
                                    <th>Prix total</th>
                                    <th>Date</th>
                                    <th>Nb jour validité</th>
                                    <th></th>
                                </tr>

                                @foreach($estimates as $estimate)
                                    <tr>
                                        <td><a href="{{route('estimate.show', $estimate->id)}}">Devis n° {{$estimate->id}}</a></td>
                                        <td>{{$estimate->rows->count()}}</td>
                                        <td>{{$estimate->price}} €</td>
                                        <td>{{date('d / m / Y', strtotime($estimate->created_at))}}</td>
                                        <td>{{$estimate->days}}</td>
                                        <td><a href="{{route('estimate.show', $estimate->id)}}" class="btn btn-default"><i class="fa fa-file" aria-hidden="true"></i> Voir le devis</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        @else
                            <strong>Le client n'a pas de devis</strong>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
