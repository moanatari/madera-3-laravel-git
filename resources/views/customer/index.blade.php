@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    Clients
                </p>
                <div class="panel panel-default">
                    <div class="panel-heading">Liste des clients</div>

                    <div class="panel-body">

                        <div class="col-md-12">
                            <p><a href="{{route('customer.create')}}" class="btn-default btn"><i class="fa fa-user" aria-hidden="true"></i> Créer un client</a></p>
                        </div>

                        @if($customers->count())
                            <table class="table table-bordered">
                                <tr>
                                    <th>Nom du client</th>
                                    <th>Ville</th>
                                    <th>Nb projet</th>
                                    <th></th>
                                </tr>

                                @foreach($customers as $customer)
                                    <tr>
                                        <td>{{$customer->name}} {{$customer->firstname}}</td>
                                        <td>{{$customer->city}}</td>
                                        <td>{{$customer->projects->count()}}</td>
                                        <td><a href="{{route('customer.show', $customer->id)}}" class="btn btn-default">Voir le client</a></td>

                                    </tr>
                                @endforeach
                            </table>
                        @else
                            <strong>Aucun client dans la base de données</strong>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
