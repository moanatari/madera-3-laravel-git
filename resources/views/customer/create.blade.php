@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('customer.index')}}">Clients</a>
                    /
                    Création d'un client
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="panel panel-default">
                    <div class="panel-heading">Créer un client</div>

                    <div class="panel-body">
                        <form action="{{route('customer.store')}}" method="POST">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="">Nom du client</label>
                                <input type="text" name="name" class="form-control" required value="{{old('name')}}">
                            </div>

                            <div class="form-group">
                                <label for="">Prenom du client</label>
                                <input type="text" name="firstname" class="form-control" required value="{{old('firstname')}}">
                            </div>

                            <div class="form-group">
                                <label for="">Téléphone</label>
                                <input type="text" name="phone" class="form-control" required value="{{old('phone')}}">
                            </div>

                            <div class="form-group">
                                <label for="">Adresse mail</label>
                                <input type="mail" name="mail" class="form-control" required value="{{old('mail')}}">
                            </div>

                            <div class="form-group">
                                <label for="">Adresse du client</label>
                                <input type="text" name="address" class="form-control" required value="{{old('address')}}">
                            </div>

                            <div class="form-group">
                                <label for="">Code postal</label>
                                <input type="text" name="postalCode" class="form-control" required value="{{old('postalCode')}}">
                            </div>

                            <div class="form-group">
                                <label for="">Ville</label>
                                <input type="text" name="city" class="form-control" required value="{{old('city')}}">
                            </div>

                            <button type="submit" class="btn btn-default">Créer le client</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
