@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('customer.index')}}">Clients</a>
                    /
                    Modification d'un client
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="panel panel-default">
                    <div class="panel-heading">Modifier un client</div>

                    <div class="panel-body">
                        <form action="{{route('customer.update', $customer->id)}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('put') }}

                            <div class="form-group">
                                <label for="">Nom du client</label>
                                <input type="text" name="name" class="form-control" required @if(!old('name')) value="{{$customer->name}}" @else value="{{old('name')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="">Prenom du client</label>
                                <input type="text" name="firstname" class="form-control" required @if(!old('firstname')) value="{{$customer->firstname}}" @else value="{{old('firstname')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="">Téléphone</label>
                                <input type="text" name="phone" class="form-control" required @if(!old('phone')) value="{{$customer->phone}}" @else value="{{old('phone')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="">Adresse mail</label>
                                <input type="mail" name="mail" class="form-control" required @if(!old('mail')) value="{{$customer->mail}}" @else value="{{old('mail')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="">Adresse du client</label>
                                <input type="text" name="address" class="form-control" required @if(!old('address')) value="{{$customer->address}}" @else value="{{old('address')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="">Code postal</label>
                                <input type="text" name="postalCode" class="form-control" required @if(!old('postalCode')) value="{{$customer->postalCode}}" @else value="{{old('postalCode')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="">Ville</label>
                                <input type="text" name="city" class="form-control" required @if(!old('city')) value="{{$customer->city}}" @else value="{{old('city')}}" @endif>
                            </div>

                            <div class="col-md-12 text-center">
                                <a href="{{route('customer.show', $customer->id)}}" class="btn btn-lg btn-default"><i class="fa fa-undo" aria-hidden="true"></i> Annuler</a>
                                <button type="submit" class="btn btn-lg btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer les modifications</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
