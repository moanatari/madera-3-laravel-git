@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p>
                Accueil
            </p>
            <div class="panel panel-default">
                <div class="panel-heading">Créer un projet</div>

                <div class="panel-body">
                    <div class="col-md-12 text-center">
                        <a class="btn btn-default btn-lg" href="{{route('project.create')}}"><i class="fa fa-home fa-lg" aria-hidden="true"></i> Créer un nouveau projet</a>
                    </div>
                </div>


            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Liste des projets</div>

                <div class="panel-body">
                    @if($projects->count())
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th>Nom du projet</th>
                                <th>Reférence du projet</th>
                                <th>Client</th>
                                <th>Date</th>
                                <th></th>
                            </tr>
                            @foreach($projects as $project)
                                <tr>
                                    <td><a href="{{route('project.show', $project->id)}}">{{$project->name}}</a></td>
                                    <td>{{$project->reference}}</td>
                                    <td>
                                        @if($project->customer_id != null)
                                            {{$project->customer->name}} {{$project->customer->firstname}}
                                        @else
                                            Pas de client
                                        @endif
                                    </td>
                                    <td>{{date('d / m / Y', strtotime($project->created_at))}}</td>
                                    <td>
                                        <a href="{{route('project.show', $project->id)}}" class="btn btn-default"><i class="fa fa-file" aria-hidden="true"></i> Voir le Projet</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p><strong>Aucun projet dans la base de données</strong></p>
                    @endif
                </div>


            </div>
        </div>
    </div>
</div>
@endsection
