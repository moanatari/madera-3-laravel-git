@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('order.show',$moduleOrder->order_id)}}">Commande</a>
                    /
                    Modification d'un module
                </p>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif



                <div class="panel panel-default">
                    <div class="panel-heading">Modifier un module de la commande</div>

                    <div class="panel-body">

                        <form action="{{route('order.updateEditedModule')}}" method="post">

                            {{ csrf_field() }}
                            <input type="text" name="moduleOrder_id" hidden  value="{{$moduleOrder->id}}" >


                            <div class="form-group">
                                <label>Description du module</label>
                                <input type="text" class="form-control" name="name" required @if(!old('name')) value="{{$moduleOrder->name}}" @else value="{{old('name')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label>Etage</label>
                                <input type="number" class="form-control" name="stage" required @if(!old('stage')) value="{{$moduleOrder->stage}}" @else value="{{old('stage')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label> {{$modules[0]->nature->unit}}</label>
                                <input type="number" class="form-control" name="length" required @if(!old('length')) value="{{$moduleOrder->length}}" @else value="{{old('length')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label>Reduction</label>
                                <input type="number" class="form-control" name="discount" required @if(!old('discount')) value="{{$moduleOrder->discount}}" @else value="{{old('discount')}}" @endif>
                            </div>

                            <div class="form-group">
                                <label>Choisir un modèle :</label>
                                <select multiple size="{{$modules->count()}}" name="module_id" class="form-control">

                                    @foreach($modules as $module)
                                        @if($moduleOrder->module_id == $module->id)
                                            <option value="{{$module->id}}" selected >{{$module->name}}</option>
                                            @else
                                            <option value="{{$module->id}}" >{{$module->name}}</option>
                                        @endif>

                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-12 text-center">
                                <a href="{{route('order.show',$moduleOrder->order_id)}}" class="btn btn-lg btn-default"><i class="fa fa-undo fa-lg" aria-hidden="true"></i> Annuler</a>
                                <button type="submit" class="btn btn-lg btn-success"><i class="fa fa-floppy-o fa-lg" aria-hidden="true"></i> Enregistrer les modifications</button>
                            </div>
                        </form>




                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
