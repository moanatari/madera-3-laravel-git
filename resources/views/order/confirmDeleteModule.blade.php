@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-8 col-md-offset-2">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('order.show',$module->order_id)}}">Commande</a>
                    /
                    Suppression d'un module
                </p>
                <div class="alert alert-danger" role="alert"><strong>Attention !</strong> Vous êtes sur le point de supprimer un module de la commande !</div>
                <div class="panel panel-default">
                    <div class="panel-heading">Supprimer un module de la commande ?</div>

                    <div class="panel-body">

                        
                    <h3>Vous allez supprimer le module : </h3>
                        <h3><strong>"{{$module->name}}"</strong></h3>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="{{route('order.show', $module->order_id)}}" class="btn btn-lg btn-default"><i class="fa fa-undo fa-lg" aria-hidden="true"></i> Annuler</a>
                                <a href="{{route('order.deleteModule', $module->id)}}" class="btn btn-lg btn-danger"><i class="fa fa-trash fa-lg" aria-hidden="true"></i> Supprimer</a>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
