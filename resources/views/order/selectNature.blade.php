@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('order.show',$order_id)}}">Commande</a>
                    /
                    Ajout d'un module
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Choisir une nature de module</div>

                    <div class="panel-body">

                        <form action="{{route('order.passCreateModule')}}" method="post">
                            {{ csrf_field() }}
                            <input type="text" name="order_id" value="{{$order_id}}" hidden required>
                            <div class="form-group">
                                <label>Choix de la nature du module</label>

                            <select name="nature_id" class="form-control">

                                @foreach($natures as $nature)
                                    <option value="{{$nature->id}}">{{$nature->name}}</option>
                                @endforeach
                            </select>
                            </div>

                            <button type="submit" class="btn btn-default">Suivant</button>
                        </form>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
