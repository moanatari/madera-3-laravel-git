@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    @if(!old('order_id'))
                        <a href="{{route('order.show', $order_id )}}">Commande</a>
                    @else
                        <a href="{{route('order.show', old('order_id'))}}">Commande</a>
                    @endif
                    /
                    Ajout d'un module
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="panel panel-default">
                    <div class="panel-heading">Ajouter un module à la commande</div>

                    <div class="panel-body">

                            <form action="{{route('order.storeCreatedModule')}}" method="post">

                                {{ csrf_field() }}
                                <input type="text" name="order_id" hidden @if(!old('order_id')) value="{{$order_id}}" @else value="{{old('order_id')}}" @endif>


                                <div class="form-group">
                                    <label>Description du module</label>
                                    <input type="text" class="form-control" name="name" required @if(!old('name')) value="{{$modules[0]->nature->name}}" @else value="{{old('name')}}" @endif>
                                </div>

                                <div class="form-group">
                                    <label>Etage</label>
                                    <input type="number" class="form-control" name="stage" required @if(!old('stage')) value="0" @else value="{{old('stage')}}" @endif>
                                </div>

                                <div class="form-group">
                                    <label>{{$modules[0]->nature->unit}}</label>
                                    <input type="text" class="form-control" name="length" required @if(!old('length')) value="0" @else value="{{old('length')}}" @endif>
                                </div>

                                <div class="form-group">
                                    <label>Choisir un modèle :</label>
                                    <select multiple size="{{$modules->count()}}" name="module_id" class="form-control" required>

                                        @foreach($modules as $module)
                                            <option value="{{$module->id}}">{{$module->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-12 text-center">
                                    <a href="{{route('order.show', $order_id)}}" class="btn btn-lg btn-default"><i class="fa fa-undo" aria-hidden="true"></i>Annuler</a>
                                    <button type="submit" class="btn btn-lg btn-success">Ajouter le module</button>
                                </div>
                            </form>




                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
