@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('project.show',$project_id)}}">Projet</a>
                    /
                    Sélection d'une commande type
                </p>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Nouvelle Commande</div>

                    <div class="panel-body">



                            <div class="form-group">
                                <label for="">Choisir un modèle de maison :</label>

                            </div>
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Gamme</th>
                                    <th>Nom</th>
                                    <th>Nb. modules</th>
                                    <th>Prix</th>
                                    <th></th>
                                </tr>
                                @foreach($modeles as $modele)
                                    <tr>
                                        <td>{{$modele->range->name}}</td>
                                        <td>{{$modele->name}}</td>
                                        <td>{{$modele->modules->count()}}</td>
                                        <td>{{$modele->getTotalPrice()}} €</td>
                                        <td>
                                            <form action="{{route('order.storeSelectedExample')}}" method="POST">
                                            {{ csrf_field() }}
                                                <input type="text" name="project_id" hidden required value="{{$project_id}}">
                                                <input type="text" name="example_id" hidden required value="{{$modele->id}}">
                                                <button type="submit" class="btn btn-default">Choisir</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
