@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <p>
                    <a href="{{route('home')}}">Accueil</a>
                    /
                    <a href="{{route('project.show',$order->project_id)}}">Projet</a>
                    /
                    Commande
                </p>

                <div class="panel panel-default">
                    <div class="panel-heading">Commande numéro {{$order->id}}</div>

                    <div class="panel-body">
                        <h2>Détail de la commande {{$order->id}}</h2>

                        <div class="row">
                            <div class="col-md-6">
                                <h3>Liste des modules :</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{route('order.selectNature', $order->id)}}" class="btn btn-default btn-lg"><i class="fa fa-edit fa-lg" aria-hidden="true"></i> Ajouter un module</a>
                            </div>
                        </div>


                        <table class="table table-bordered">
                            <tr>
                                <th>Etage</th>
                                <th>Description du module</th>
                                <th>Nature du module</th>
                                <th>Qte</th>
                                <th>Unité</th>
                                <th>Prix Unitaire</th>
                                <th>Total</th>
                                <th></th>

                            </tr>
                            @if(!$order->modules->count())
                            <tr>
                                <td colspan="8"><strong>Il n'y a aucun module dans cette commande.</strong></td>

                            </tr>
                            @endif
                            @foreach($order->modules as $module)
                                <tr>
                                    <td>@if($module->pivot->stage == 0)RDC @else {{$module->pivot->stage}} @endif</td>
                                    <td>{{$module->pivot->name}}</td>
                                    <td>{{$module->nature->name}}</td>
                                    <td>{{$module->pivot->length}}</td>
                                    <td>{{$module->nature->unit}}</td>
                                    <td>{{$module->getUnitPrice()}} €</td>
                                    <td>@if($module->pivot->discount != 0)<strong>*</strong>@endif{{$module->getModulePrice()}} €</td>
                                    <td>
                                        <a href="{{route('order.editModule',$module->pivot->id)}}" class="btn btn-default">
                                            <i class="fa fa-pencil fa-lg" aria-hidden="true"></i>
                                        </a>
                                        <a href="{{route('order.confirmDeleteModule',$module->pivot->id)}}" class="btn btn-danger">
                                            <i class="fa fa-trash fa-lg" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="4"></td>
                                <td colspan="2"><strong>TOTAL :</strong></td>
                                <td colspan="2"><strong>{{$order->getTotalPrice()}} €</strong></td>
                            </tr>
                        </table>

                        @if($order->project->customer_id != null)
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <a href="{{route('estimate.validateEstimate', $order->id)}}" class="btn btn-default btn-lg"><i class="fa fa-file" aria-hidden="true"></i> Créer un devis pour cette commande</a>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <div class="alert alert-info" role="alert">Le projet n'est lié à aucun client, impossible de créer le devis.</div>

                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
